#!/usr/bin/env python
from lxml.builder import E
from lxml import etree
import cgi
import psycopg2


class Db():
    """
        Database handling
    """

    dbhost = 'attorney.clxffukfikcy.us-west-1.rds.amazonaws.com'
    dbport = 5432
    dbusername = 'kamailio'
    dbpassword = 'MYA$terisk2011!'
    dbname = 'attorney'

    # =========================================================
    # SQLALCHEMY
    # =========================================================

    # "postgresql://telephonist:telephonist@198.199.110.24/telephonistdb
    SQLALCHEMY_DATABASE_URI = "postgresql://" + dbusername + ":" + dbpassword + "@" + dbhost + "/" + dbname
    SQLALCHEMY_DSN = 'dbname=' + dbname + \
                     ' host=' + dbhost + \
                     ' port=' + str(dbport) + \
                     ' user=' + dbusername + \
                     ' password=' + dbpassword

    def __init__(self,
                 server=dbhost,
                 username=dbusername,
                 password=dbpassword,
                 database=dbname,
                 port=dbport, **kwargs):
        """

        :param server:
        :param username:
        :param password:
        :param database:
        :param port:
        :param kwargs:
        :return:
        """
        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.dsn = 'dbname=%s host=%s port=%d user=%s password=%s' % (self.database,
                                                                      self.server,
                                                                      self.port,
                                                                      self.username,
                                                                      self.password)
        self.conn = None

    def initialize(self, dsn=None, **kwargs):
        """

        :param dsn:
        :param kwargs:
        :return:
        """

        if self.dsn:
            self.conn = psycopg2.connect(self.dsn)
            return True
        elif dsn:
            self.conn = psycopg2.connect(dsn)
            return True
        else:
            print ('DB initialize() DB not initialized')
            raise RuntimeError('Invalid dsn.')

    def query_multiple(self,query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                # A commit may go here...
                result = cur.fetchone()
                if result:
                    return result
            else:
                print 'DB.query_multiple() Invalid db parameters'
                return

        except psycopg2.ProgrammingError as exception:
            print 'DB.query_multiple() Database configuration: {}'.format(exception)
            raise
        except Exception as exception:
            print 'DB.query_multiple() Unable to query: {}'.format(exception)
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

def QueryDatabase(sqlquery):
    """

    :param sqlquery:
    :return:
    """
    try:
        db = Db()
        db.initialize()
        return db.query_multiple(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def CreateBaseDirectory():
    """

    :return:
    """
    doc = (
        E.document(
            E.section(name="directory"), type="freeswitch/xml")
    )
    return doc


def AddDirectoryUser(doc, domain, username):
    """

    :param doc:
    :param domain:
    :param username:
    :param password:
    :param vm_password:
    :return:
    """
    password_param = "password"
    vm_password_param = "vm-password"
    mailbox_param = "mailbox"
    user_context_param = "user_context"
    # comment out the line below to test with plain text passwords
    section = doc.find("section")

    # search for a domain tag for the indicated domain
    # if the domain is not found, add it
    searchStr = 'domain[@name="{}"]'.format(domain)
    results = section.xpath(searchStr)
    if len(results) > 0:
        dom = results[0]
    else:
        dom = (
            E.domain(
                E.params(
                    E.param(
                        name="dial-string",
                        value='{presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(${dialed_user}@${dialed_domain})}'
                    )
                ),
                E.groups(
                )
                , name=domain)
        )
        section.append(dom)

        # search for a group tag (for the "default" context)
        # if the group is not found, add it

        groups = dom.find("groups")
        searchStr = 'group[@name="{}"]'.format("default")
        results = groups.xpath(searchStr)
        if len(results) > 0:
            grp = results[0]
        else:
            grp = E.group(
                E.users()
                , name="default")
            groups.append(grp)

        # add the new user #<variable name="user_context" value="default"/>
        sqlquery = """SELECT phone_number, vm_password FROM persons WHERE phone_number='""" + username + "'"
        person = QueryDatabase(sqlquery)

        if person:
            if len(person) == 2:
                phone_number = person[0]
                vm_password = person[1]

        grp.find("users").append(
            E.user(
                E.params(
                    E.param(name=password_param, value=username),
                    E.param(name=vm_password_param, value=vm_password)),
                E.variables(
                    E.var(name=user_context_param, value="default"),
                    E.var(name=mailbox_param, value=phone_number)),
                id=username),
        )



document = CreateBaseDirectory()
AddDirectoryUser(document, "104.236.190.206", '14082186575')

print "Content-Type: text/xml"
print
print(etree.tostring(document, pretty_print=True))