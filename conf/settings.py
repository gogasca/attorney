import platform

if platform.system() == 'Linux':
    filepath = '/usr/local/src/attorney/'
else:
    filepath = '/Users/gogasca/Documents/OpenSource/Development/Python/attorney/'

BASE_URL = 'http://104.131.146.219:8081' + '/api/1.0'
SPARK_URL = 'https://api.ciscospark.com/v1/'
TWILIO_FROM = '+16506814252'
TWILIO_ACCOUNT = 'AC64861838b417b555d1c8868705e4453f'
TWILIO_TOKEN = 'a7f130908851f1e956354149fff68150'
USER_NOT_LICENSED = 'USER_NOT_LICENSED'
USER_REGISTERED = 'USER_REGISTERED'
USER_NOT_REGISTERED = 'USER_NOT_REGISTERED'
USER_INVALID_REGISTRATION = 'USER_INVALID_REGISTRATION'
PARZEE_BOT = 'parzee@sparkbot.io'
PARZEE_URI = '@sip.parzee.io'
PARZEE_ERROR_URI = 'sip:error@sip.parzee.io'
SIP_SCHEME = 'sip:'
SPARK_TOKEN = 'Bearer Nzc3ZGU1MjgtZTM3Mi00YjFlLWJiOTUtNGY0NzBlOGIzODhhNGFjZWQwZmQtMmVh'
BOT_TOKEN = 'Bearer OTI0MWNmMDYtYWE3MS00NTE0LTk3YzAtODRjMDdjNWE5MmJhMGM5ZDZhMjUtN2Yz'
BOT_WEBHOOK = 'Y2lzY29zcGFyazovL3VzL1dFQkhPT0svN2IyM2U2MzUtOTNiYS00NjNhLWIwZDEtOTNiMGM5MDRlYjYy'
BOT_WEBHOOK_CALLBACK = BASE_URL + '/webhook'

call_default_country = '1'
call_parzee_voicemail = 'sip:voicemail@sip.parzee.io'
call_params = ';transport=tls'

spark_info_help = 'Available commands: /register | /call <phone> | /help | /conference <Phone A> <Phone B>'
spark_info_connecting = 'Connecting call...'
spark_error_reply_destination = 'Invalid destination. Usage: /call <phone number>'
spark_error_user_not_registered = 'Please complete Spark registration. Call registration@sip.parzee.io and use verification code: '
spark_error_code_system_down = 'Registration system is unavailable. Please try again later'
spark_error_user_invalid_uri = 'Invalid user configuration. Please contact Technical support'
spark_error_user_not_licensed = 'User is not licensed. Please contact support@parzee.com'
spark_error_user_already_registered = 'User is already registered. You can place calls now!'
spark_call_completed = 'Call completed'
spark_call_failed= 'Call failed'


freeswitch_host = '104.236.190.206'
freeswitch_web = 'http://voicemail.parzee.io'
fs_xml_rpc_host = '127.0.0.1'
fs_xml_rpc_port = '8082'
fs_xml_rpc_uname = 'freeswitch'
fs_xml_rpc_auth = 'M1Nub3'

# =========================================================
# API
# =========================================================

valid_providers = ['twilio', 'sip']
sip_domain = 'sip.parzee.io'

max_message_processing = 3600
max_call_duration = 28800
api_logfile = filepath + '/log/apid.log'
api_frontend = 'loadbalancer'
api_external_port = 8080
api_external_recording_port = 8081
api_scheme = 'http'
api_url = api_scheme + '://' + api_frontend + ':' + str(api_external_port) + '/api/1.0'
api_call_request = api_url + '/call'
api_version = '0.1'
api_base_url = '/api/1.0/'
api_account = 'AC64861838b417b555d1c8868705e4453f'
api_password = 'YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o'
api_ip_address = '0.0.0.0'
api_port = 8081
items_per_page = 10
api_error_limit = 50

# =========================================================
# Database
# =========================================================
# psql -h 198.199.110.24 -d telephonistdb -U telephonist -W
dbhost = 'attorney.clxffukfikcy.us-west-1.rds.amazonaws.com'
dbport = 5432
dbusername = 'kamailio'
dbpassword = 'MYA$terisk2011!'
dbname = 'attorney'
dbPasswordAllowEmptyPassword = False

# =========================================================
# SQLALCHEMY
# =========================================================

# 'postgresql://telephonist:telephonist@198.199.110.24/telephonistdb
SQLALCHEMY_DATABASE_URI = 'postgresql://' + dbusername + ':' + dbpassword + '@' + dbhost + '/' + dbname
SQLALCHEMY_DSN = 'dbname=' + dbname + \
                 ' host=' + dbhost + \
                 ' port=' + str(dbport) + \
                 ' user=' + dbusername + \
                 ' password=' + dbpassword
SQLALCHEMY_KAMAILIO = 'dbname=' + 'kamailiodb' + \
                 ' host=' + dbhost + \
                 ' port=' + str(dbport) + \
                 ' user=' + dbusername + \
                 ' password=' + dbpassword
SQLALCHEMY_ECHO = False
SQLALCHEMY_POOL_SIZE = 1024
SQLALCHEMY_POOL_TIMEOUT = 5
SQLALCHEMY_MAX_OVERFLOW = 0
SQLALCHEMY_POOL_RECYCLE = 60
SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = None
SQLALCHEMY_COMMIT_ON_TEARDOWN = True
