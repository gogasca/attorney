import platform
import sys

from kombu import Exchange, Queue

import settings

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/attorney')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/attorney/')

CELERYD_CHDIR = settings.filepath
CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = "US/Eastern"
CELERY_ACCEPT_CONTENT = ['json', 'pickle', 'yaml']
CELERY_IGNORE_RESULT = True
CELERY_RESULT_BACKEND = "amqp"
CELERY_RESULT_PERSISTENT = True
BROKER_URL = 'amqp://attorney:attorney@rabbitmq:5672'
BROKER_CONNECTION_TIMEOUT = 15
BROKER_CONNECTION_MAX_RETRIES = 5
CELERY_DISABLE_RATE_LIMITS = True
CELERY_TASK_RESULT_EXPIRES = 7200
CELERY_IMPORTS = ("callcontrol.attorney")

CELERY_DEFAULT_QUEUE = "default"
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    Queue('gold', Exchange('attorney'), routing_key='attorney.gold'),
    Queue('silver', Exchange('attorney'), routing_key='attorney.silver'),
    Queue('bronze', Exchange('attorney'), routing_key='attorney.bronze'),
)
CELERY_DEFAULT_EXCHANGE = "attorney"
CELERY_DEFAULT_EXCHANGE_TYPE = "topic"
CELERY_DEFAULT_ROUTING_KEY = "default"
CELERY_TRACK_STARTED = True

CELERY_ROUTES = {
    'process_message': {'queue': 'gold', 'routing_key': 'attorney.gold', 'exchange': 'attorney',},
    'process_call'   : {'queue': 'silver', 'routing_key': 'attorney.silver', 'exchange': 'attorney',},
}
