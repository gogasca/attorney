import xmlrpclib
import logging
from conf import logger
from conf import settings

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class XMLRpcClientImpl(object):
    """

    """

    def __init__(self,
                 host=settings.fs_xml_rpc_host,
                 port=settings.fs_xml_rpc_port,
                 uname=settings.fs_xml_rpc_uname,
                 auth=settings.fs_xml_rpc_auth):
        """

        :param host:
        :param port:
        :param uname:
        :param auth:
        :return:
        """
        self.server = xmlrpclib.ServerProxy('http://%s:%s@%s:%s' % (uname, auth, host, port))

    def send_command(self, command, params):
        log.info('send_command() {} {}'.format(command, params))
        res = self.server.freeswitch.api(command, params)
        if res:
            return res
