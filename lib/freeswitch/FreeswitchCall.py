import sys
import platform

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/attorney')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/freeswitch.git/libs/esl/python')

import ESL
import time


class FreeSwitchCall(object):
    def __init__(self):
        pass

    def dial(self, destination):
        """
        CallA Invalid           CallB [Rings, Answer]
        CallA [Rings, Answer]   CallB Invalid
        CallA Answers           CallB Rings
        CallA Rings             CallB Answers
        CallA Answers           CallB Answers
        CallA Hangup            -
        -                       CallB Hangup
        :param destination:
        :return:
        """

        esl_instance = ESL.ESLconnection('104.236.190.206', '8021', 'ClueCon')
        if esl_instance.connected():
            esl_instance.events("plain",
                                "CHANNEL_OUTGOING CHANNEL_STATE CHANNEL_CREATE CHANNEL_PROGRESS_MEDIA CHANNEL_CALLSTATE BACKGROUND_JOB CHANNEL_HANGUP")
        else:
            print 'dial() ESL instance not connected'
            return

        uuid = esl_instance.api("create_uuid").getBody()

        print 'dial() Created UUID: {}'.format(uuid)
        command = "originate {ignore_early_media=false,bridge_early_media=false,origination_uuid=" + \
                  uuid + ",originate_timeout=18,origination_caller_id_number=+18623079305}sofia/gateway/twilio/" + \
                  destination[1] + " handle_calls"

        res = esl_instance.bgapi(command)
        job_uuid = res.getHeader("Job-UUID")

        print 'dial() Call to: ' + command + ' has Job-UUID {} Call-UUID {}'.format(job_uuid, uuid)

        print 'dial() Contacting Freeswitch checking UUID...'
        uuid_exists = esl_instance.api('uuid_exists ' + uuid).getBody()
        time.sleep(0.25)
        if not uuid_exists:
            print 'dial() Call has not been created UUID {}'.format(uuid)
            return
        else:
            print 'dial() UUID created: {}'.format(uuid)
