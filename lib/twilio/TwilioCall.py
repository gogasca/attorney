import logging
import time

from twilio.rest import TwilioRestClient

from conf import settings, logger
from twilio import TwilioException
from twilio import TwilioRestException

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


def twilio_helper(message_instance, spark_client, destination, call_type):
    """
    This helper function uses Twilio api to create conference call between Spark client and destination number
    :param message_instance:
    :param spark_client:
    :param destination:
    :param call_type:
    :return:
    """
    try:
        client = TwilioRestClient(account=settings.TWILIO_ACCOUNT, token=settings.TWILIO_TOKEN)
        twilio_instance = TwilioCall(client)
        twilio_instance.call_type = call_type
        dial_response = twilio_instance.dial(spark_client, destination)
        if dial_response:
            res = twilio_instance.check_progress()
            result = 'completed'
            if res:
                message_instance.send_notification(settings.spark_call_completed)
        else:
            result = 'failed'
            message_instance.send_notification(settings.spark_call_failed)
        log.info(result)
    except TwilioRestException, exception:
        log.exception("twilio_helper {}".format(exception))
    except TwilioException, exception:
        log.exception("twilio_helper {}".format(exception))


class TwilioCall(object):
    """

    """

    def __init__(self, client):
        self._callsid = None
        self._client = client
        self._duration = 3600
        self._call_type = 1

    @property
    def call_type(self):
        return self._call_type

    @call_type.setter
    def call_type(self, call_type):
        self._call_type = call_type

    @property
    def callsid(self):
        return self._callsid

    @callsid.setter
    def callsid(self, callsid):
        self._callsid = callsid

    @property
    def client(self):
        return self._client

    @client.setter
    def client(self, client):
        self._client = client

    @property
    def duration(self):
        return self._duration

    @duration.setter
    def duration(self, duration):
        self._duration = duration

    def dial(self, spark_client, _destination):
        """

        :param spark_client:
        :param _destination:
        :return:
        """
        try:
            if _destination is None:
                log.exception('dial() Invalid destination')
                return

            log.info("dial() {}".format(spark_client))
            if self.client:
                log.info('dial() Creating call via API...')

                if self.call_type == 1:
                    call_type = 'Pstn'
                if self.call_type == 2:
                    call_type = 'SipUri'

                # Encode country code
                if _destination[:1] == '+':
                    _destination = _destination[1:]

                # Send call to Parzee URL
                parzee_url = settings.BASE_URL + '/call?' + call_type + '=' + _destination + '&' + 'Country=' + \
                             settings.call_default_country
                log.info('dial() Sending call to Parzee API: {}'.format(parzee_url))

                # Send call to 'Twilio' API, for callback status use /call which is part of the API.
                call = self.client.calls.create(to=spark_client,
                                                from_=settings.TWILIO_FROM,
                                                url=parzee_url,
                                                status_callback=settings.BASE_URL + '/callstatus',
                                                status_callback_method="POST",
                                                timeout=20)

                log.info('dial() New Call CallSid: {}'.format(call.sid))
                self.callsid = call.sid
                return True
            else:
                log.error('dial() Client not defined.')
        except TwilioRestException, exception:
            log.exception('Exception found in Twilio API {}'.format(exception))

    def status(self):
        """
        Get Twilio Call status
        :return:
        """
        call = self._client.calls.get(self.callsid)
        if call:
            log.info('status() Call Status: {}|{}'.format(self.callsid, call.status))
            return call.status
        else:
            log.info('status() Unable to get call status')

    def check_progress(self):
        """

        :return:
        """
        duration_check = 1

        while True:
            if duration_check <= self.duration:
                status = self.status()
                if status:
                    log.info('check_progress() {}|{}'.format(self.callsid, status))
                else:
                    log.info('check_progress() Not able to get status information')
                if status == 'completed':
                    log.info('check_progress() Call completed')
                    break
                if status == 'failed':
                    log.error('check_progress() Call failed')
                    break
                if status == 'busy':
                    log.error('check_progress() Call busy')
                    break
                if status == 'no-answer':
                    log.info('check_progress() Call no-answer')
                    break
                # We introduce a pause before we check the status again.
                time.sleep(2)
                duration_check += 1
            else:
                log.info('check_progress() Total call duration reached {}'.format(duration_check))
                self.hangup()
                break

        print status, self.callsid
        return True

    def hangup(self):
        """

        :return:
        """
        if self.callsid and self.client:
            self.client.calls.hangup(self.callsid)
            log.info('hangup() Call ended: {}'.format(self.callsid))
            return True
        else:
            log.error('hangup() Unable to end call'.format(self.callsid))
            return False
