import random
import string
import uuid


class Generator(object):
    """
    Create new Tools server. Tools server creates floppy image.
    """

    def __init__(self):
        """

        :return:
        """
        pass

    def generate_job(self, job_length):
        """
        :param job_length:
        :return:
        """

        return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(job_length))

    def get_uuid(self):
        """

        :return:
        """
        return (str(uuid.uuid1()).upper().replace("-", "")[:8].lower() + '-' + \
                self.generate_key(4, 4) + '-' + \
                str(uuid.uuid1()).upper().replace("-", "")[:10].lower()).lower()

    def get_registration_code(self):
        """

        :return:
        """
        return ''.join(random.SystemRandom().choice(string.digits) for _ in range(4))

    def generate_key(self, parts, job_length):
        """

        :param parts:
        :param job_length:
        :return:
        """
        if parts > 0 and job_length > 0:
            key = ''
            for y in xrange(0, parts):
                key += ''.join(
                    random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(job_length))
                key += '-'

            # Remove last characters
            return key[:-1]
        else:
            return None
