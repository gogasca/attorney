from conf import settings


def get_destination(country='1', destination=''):
    """

    :param country:
    :param destination:
    :return:
    """
    if country == '1':
        if len(destination) == 10:
            return '+1' + destination
        elif len(destination) == 11:
            return '+' + destination
        elif len(destination) == 12 and destination[:1] == '+':
            return destination[1:]
        else:
            return settings.PARZEE_ERROR_URI + settings.call_params
