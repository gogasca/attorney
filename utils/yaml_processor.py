import yaml


class YamlProcessor(object):
    """

    """

    def __init__(self, f):
        self.filename = f

    def get_property(self, root, property):
        with open(self.filename, 'r') as f:
            doc = yaml.load(f)
        try:
            for element in doc[root]:
                if property == element:
                    return doc[root][property]
        except KeyError:
            return None
        except Exception, e:
            print str(e)