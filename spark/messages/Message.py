from conf import settings
import requests


class Message(object):
    def __init__(self, id=None):
        self._url = settings.SPARK_URL + 'messages'
        self._id = id
        self._text = None
        self._person_id = None
        self._person_email = None
        self._room_id = None
        self._user = None

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    def get_message(self, mode=1):
        """

        :param mode:
        :return:
        """
        try:
            if mode == 1:
                token = settings.BOT_TOKEN
            if mode == 2:
                token = settings.SPARK_TOKEN

            # add authorization to the header
            header = {"Authorization": "%s" % token}

            # create request url using message ID
            get_rooms_url = self._url + '/' + self.id

            # send the GET request and do not verify SSL certificate for simplicity of this example
            api_response = requests.get(get_rooms_url, headers=header, verify=False)

            # parse the response in json
            response_json = api_response.json()

            # get the text value from the response
            text = response_json["text"]

            # return the text value
            return text
        except Exception, exception:
            print exception

    def send_notification(self, text, markdown=False, mode=1):
        """

        :param text:
        :param markdown:
        :param mode:
        :return:
        """
        if mode == 1:
            token = settings.BOT_TOKEN
        if mode == 2:
            token = settings.SPARK_TOKEN

        # add authorization to the header
        header = {"Authorization": "%s" % token, "content-type": "application/json"}

        # create message in Spark room
        if markdown:
            payload = {
                "personId"   : self._person_id,
                "personEmail": self._person_email,
                "roomId"     : self._room_id,
                "markdown"   : text
            }
        else:
            payload = {
                "personId"   : self._person_id,
                "personEmail": self._person_email,
                "roomId"     : self._room_id,
                "text"       : text,
            }

        # create POST request do not verify SSL certificate for simplicity of this example
        api_response = requests.post(self._url, json=payload, headers=header, verify=False)

        # get the response status code
        response_status = api_response.status_code

        # return the text value
        print(response_status)

    def post_message(self, person_id, person_email, room_id, text, mode=1):
        """

        :param person_id:
        :param person_email:
        :param room_id:
        :param text:
        :param mode:
        :return:
        """
        # define a variable for the hostname of Spark
        hostname = ""

        # login to developer.ciscospark.com and copy your access token here
        # Never hard-code access token in production environment
        if mode == 1:
            token = settings.BOT_TOKEN
        if mode == 2:
            token = settings.SPARK_TOKEN

        # add authorization to the header
        header = {"Authorization": "%s" % token, "content-type": "application/json"}

        # create message in Spark room
        payload = {
            "personId"   : person_id,
            "personEmail": person_email,
            "roomId"     : room_id,
            "text"       : text
        }

        # create POST request do not verify SSL certificate for simplicity of this example
        api_response = requests.post(self._url, json=payload, headers=header, verify=False)

        # get the response status code
        response_status = api_response.status_code

        # return the text value
        print(response_status)
