from api.version1_0.database import DbHelper
import json
import requests
from conf import settings


class Room(object):
    def __init__(self):
        self.id = None
        self._url = settings.SPARK_URL + 'rooms'

    def get(self, teamId=None, roomType=None, max=10):

        headers = {"Authorization": "%s" % settings.BOT_TOKEN, "content-type": "application/json; charset=utf-8"}
        payload = {'max': max}
        if teamId:
            payload['teamId'] = teamId
        if roomType:
            payload['type'] = roomType
        # print (payload)
        resp = requests.get(self._url, params=payload, headers=headers)
        room_dict = json.loads(resp.text)
        room_dict['statuscode'] = str(resp.status_code)
        return room_dict

    @staticmethod
    def get_rooms(email=None, telephone=None):
        """

        :param email:
        :param telephone:
        :return:
        """
        if telephone:
            sqlquery = """SELECT email, spark_id FROM persons WHERE telephone='""" + telephone + "'"
        if email:
            sqlquery = """SELECT email, spark_id FROM persons WHERE email='""" + email + "'"
        query_result = DbHelper.query_multiple_database(sqlquery)
        if query_result:

            if len(query_result) == 2:
                email = query_result[0]
                person_id = query_result[1]
                room_instance = Room()
                room_dict = room_instance.get(roomType='direct', max=25)
                for room in room_dict['items']:
                    if person_id == room['creatorId']:
                        return person_id, email, room['id']
