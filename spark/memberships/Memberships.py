from conf import settings
import json
import requests


class Memberships(object):
    def __init__(self):
        self._url = settings.SPARK_URL + 'memberships'

    def get(self, roomId=None, personId=None, personEmail=None, max=10):
        """

        :param roomId:
        :param personId:
        :param personEmail:
        :param max:
        :return:
        """
        token = settings.BOT_TOKEN
        # add authorization to the header
        header = {"Authorization": "%s" % token}

        # disable warnings about using certificate verification
        requests.packages.urllib3.disable_warnings()

        payload = {'max': max}
        if roomId:
            payload['roomId'] = roomId
        if personId:
            payload['personId'] = personId
        if personEmail:
            payload['personEmail'] = personEmail

        # send GET request and do not verify SSL certificate for simplicity of this example
        resp = requests.get(self._url, params=payload, headers=header, verify=True)
        membership_dic = json.loads(resp.text)
        membership_dic['statuscode'] = str(resp.status_code)
        return membership_dic