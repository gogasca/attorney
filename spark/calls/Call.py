from utils import Generator


class Call(object):
    def __init__(self):
        self._uuid = Generator.Generator().get_uuid()
        self._id = id

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        self._uuid = uuid
