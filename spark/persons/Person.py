from utils import Generator
from api.version1_0.database import DbHelper
import json
import logging
import requests
import time
from conf import settings, logger

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


class Person(object):
    def __init__(self):
        self._url = settings.SPARK_URL + 'people'
        self.id = None
        self.spark_email = None
        self.spark_uri = None
        self.parzee_uri = None
        self.code = None

    def get(self, email=None, display_name=None, max=10):
        """

        :param email:
        :param displayName:
        :param max:
        :return:
        """
        headers = {"Authorization": "%s" % settings.BOT_TOKEN, "content-type": "application/json; charset=utf-8"}
        payload = {'max': max}
        if email:
            payload['email'] = email
        if display_name:
            payload['displayName'] = display_name
        resp = requests.get(self._url, params=payload, headers=headers)
        people_dict = json.loads(resp.text)
        people_dict['statuscode'] = str(resp.status_code)
        return people_dict

    def register(self):
        """
        Register new code for user. Invalidates previous codes by setting value to -1
        :return:
        """
        if self.spark_email:
            self.id = DbHelper.query_database(sqlquery="SELECT id FROM persons WHERE email='" + self.spark_email + "'")

        if self.id:
            # Invalidate older codes.
            log.warn('register() Invalidating previous codes')
            sqlquery = """UPDATE registrations SET status=-1 WHERE person_id=""" + str(self.id) + " AND status=0"
            DbHelper.update_database(sqlquery)

            for x in range(0, 5):
                # Create a random Registration code
                registration_code = Generator.Generator().get_registration_code()
                codes = DbHelper.query_database(
                    sqlquery="SELECT COUNT(id) FROM registrations WHERE code='" + registration_code +
                             "' and person_email='" + self.spark_email + "'")
                # Code is unique
                if codes == 0:
                    result = DbHelper.insert_to_database(
                        sqlquery='INSERT INTO registrations (code, person_id, creation, status, person_email)',
                        content="'" + str(registration_code) + "','" + str(
                            self.id) + "', 'now()', 0, '" + self.spark_email + "'")
                    if result:
                        return registration_code
                # Try to generate a new code
                else:
                    # Generate a new code. Try 5 times.
                    time.sleep(0.25)

        return -1
