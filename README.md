# Overview

Allows interaction for Cisco Spark calls.
By processing text input from Cisco Spark Web hook, handle actions:
/call, /text/ etc.

Start

```
apt-get update
apt-get install python build-essential python-setuptools swig -y
apt-get install python-pip python-dev -y
apt-get install libpq-dev python-dev
apt-get install git -y

pip install coverage
pip install flower


vim .bashrc
vim ~/.profile
        
export C_FORCE_ROOT="true"

git clone git://github.com/sangoma/switchy.git
pip install -r switchy/requirements-doc.txt switchy/

pip install --upgrade pip
pip install -r requirements.txt
mkdir -p /etc/supervisor/conf.d

cp /usr/local/bin/supervisorctl /usr/bin/
cp /usr/local/bin/supervisord /usr/bin/

```

#Start supervisor after reboot:

supervisord -c supervisord.conf    

Create directory for log file
mkdir /var/log/supervisor/
    
#API
##Starting API

gunicorn telephonist_api:api_app --bind 0.0.0.0:8080 --log-level=DEBUG -w 1

#Twilio
Voice Configuration
REQUEST URL http://93c23b3d.ngrok.io/api/1.0/

#Celery
celery worker -n %h -P processes -c 15 --loglevel=DEBUG -Ofair

#Ngrok

#Database
##Import database


```
psql -d template1
psql (9.5.3)
Type "help" for help.

template1=# CREATE DATABASE attorney WITH OWNER attorney ENCODING 'UTF8';
CREATE DATABASE
template1=# grant all privileges on database attorney to attorney;
GRANT
template1=# ALTER USER "attorney" WITH PASSWORD 'attorney';
ALTER ROLE
template1=# 


CREATE TABLE calls
(
    id    serial primary key,
    callsid        VARCHAR(128) not null,
);
```

#RabbitMQ

```

rabbitmqctl add_user attorney attorney
rabbitmqctl set_user_tags attorney administrator
rabbitmqctl set_permissions -p / attorney ".*" ".*" ".*"

```

Freeswitch Voicemail system
Configure 
vim autoload_configs/xml_rpc.conf.xml


## Call

```
This is the flow when user types /call in the Sparkbot
User enters: /call <>
twilio_helper <destination spark uri>
twiliocall.dial <destination spark uri>
/call
create conference <destination pstn>
/status


```