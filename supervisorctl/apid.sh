#!/bin/bash
cd /usr/local/src/attorney/api/version1_0/
GUNICORN_LOGFILE=/usr/local/src/attorney/log/gunicorn.log
NUM_WORKERS=2
TIMEOUT=60
WORKER_CONNECTIONS=1000
BACKLOG=500
exec gunicorn attorney_api:api_app --bind 0.0.0.0:8081 --log-level=CRITICAL --log-file=$GUNICORN_LOGFILE --workers $NUM_WORKERS --worker-connections=$WORKER_CONNECTIONS --backlog=$BACKLOG --timeout $TIMEOUT
