import logging
import re

from celery.task import task

from api.version1_0.database import DbHelper
from callcontrol.modules.registration import check_registration
from callcontrol.modules.voicemail import VoiceMail
from conf import settings, logger
from lib.twilio.TwilioCall import twilio_helper
from spark.persons.Person import Person

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


@task(name='process_message', queue='gold', bind=True, default_retry_delay=30, max_retries=3,
      soft_time_limit=settings.max_message_processing)
def process_message(self, message_instance, webhook_notification):
    """

    :param destination:
    :return:
    """
    try:

        if re.match('/call\s+|/voicemail|/inbox|/register|/activate\s+|/conference\s+|/help', message_instance.text):
            print message_instance.text
            message_instance._person_id = webhook_notification['person_id']
            message_instance._person_email = webhook_notification['person_email']
            message_instance._room_id = webhook_notification['room_id']
            user_status = check_registration.get_user_status(message_instance._person_email)

            if user_status == settings.USER_NOT_LICENSED:
                message_instance.send_notification(
                    '<' + message_instance._person_email + '>' + settings.spark_error_user_not_licensed)
                return

            if user_status == settings.USER_NOT_REGISTERED:
                # Prompt user for registration code.
                person_instance = Person()
                person_instance.spark_email = message_instance._person_email
                code = person_instance.register()
                # Get Persons and get person_id
                persons = person_instance.get(email=person_instance.spark_email, max=1)
                for person in persons['items']:
                    if person['id']:
                        sqlquery = """UPDATE persons SET spark_id='""" + person['id'] + """' WHERE email='""" + \
                                   person_instance.spark_email + "'"
                        DbHelper.update_database(sqlquery)

                if code != -1:
                    message_instance.send_notification(settings.spark_error_user_not_registered + code)
                else:
                    message_instance.send_notification(settings.spark_error_code_system_down)

                return

            if '/register' in message_instance.text:
                log.info('process_message() | Activation Request: ' + str(webhook_notification['message_id']))
                if user_status == settings.USER_REGISTERED:
                    message_instance.send_notification(settings.spark_error_user_already_registered)
                return

            if '/call' in message_instance.text:

                log.info('process_message() | Call Request:  {}'.format(webhook_notification['message_id']))
                # USER already registered
                if user_status == settings.USER_REGISTERED:
                    person_instance = Person()
                    sqlquery = """SELECT parzee_uri FROM persons WHERE email='""" + message_instance._person_email + "' LIMIT 1"
                    person_instance.parzee_uri = DbHelper.query_database(sqlquery)

                    destination_match = re.match('/call\s+((\+?\d{10,24})|(sip:\+?\w+@[\w.-]+.\w{2,4})).*',
                                                 message_instance.text)

                    if person_instance.parzee_uri:
                        # USER Check call information
                        if destination_match:
                            if destination_match.group(2):
                                log.info(
                                    'process_message() | PSTN Call destination: {}'.format(destination_match.group(2)))
                                destination = destination_match.group(2)
                                call_type = 1
                            elif destination_match.group(3):
                                log.info(
                                    'process_message() | SIP URI destination: {}'.format(destination_match.group(3)))
                                destination = destination_match.group(3)
                                call_type = 2
                            else:
                                log.error('process_message() | Destination not found')
                                message_instance.send_notification(settings.spark_error_reply_destination)
                                return
                        else:
                            log.error('process_message() | Destination not found')
                            message_instance.send_notification(settings.spark_error_reply_destination)
                            return

                        log.info('api() | Spark client URI: {} Destination: {}'.format(person_instance.parzee_uri,
                                                                                       destination))

                        message_instance.send_notification(settings.spark_info_connecting)
                        #  Send call to Twilio API
                        process_call(message_instance,
                                     person_instance.parzee_uri + settings.call_params,
                                     destination, call_type)
                    else:
                        log.error('api() | Invalid SIP URI: {}'.format(person_instance.parzee_uri))
                        message_instance.send_notification(settings.spark_error_user_invalid_uri)
                return
            if '/voicemail' in message_instance.text:
                if user_status == settings.USER_REGISTERED:
                    person_instance = Person()
                    sqlquery = """SELECT parzee_uri FROM persons WHERE email='""" + message_instance._person_email + "' LIMIT 1"
                    person_instance.parzee_uri = DbHelper.query_database(sqlquery)

                    if person_instance.parzee_uri:
                        # USER Check call information

                        log.info('api() | SIP URI: {} Destination: {}'.format(person_instance.parzee_uri,
                                                                              settings.call_parzee_voicemail))
                        message_instance.send_notification(settings.spark_info_connecting)
                        #  Send call to Twilio API
                        process_call(message_instance,
                                     person_instance.parzee_uri + settings.call_params,
                                     settings.call_parzee_voicemail + settings.call_params, 2)
                    else:
                        log.error('api() | Invalid SIP URI: {}'.format(person_instance.parzee_uri))
                        message_instance.send_notification(settings.spark_error_user_invalid_uri)

            if '/help' in message_instance.text:
                message_instance.send_notification(settings.spark_info_help)
                return

            if '/conference' in message_instance.text:  # TODO
                if user_status == settings.USER_REGISTERED:
                    log.info('process_message() | Conference Request: ' + str(webhook_notification['message_id']))
                    message_instance.send_notification('Not implemented')
                return

            if '/inbox' in message_instance.text:  # TODO pass inbox type new unread saved
                # Display Inbox and new items
                if user_status == settings.USER_REGISTERED:
                    sqlquery = """SELECT telephone FROM persons WHERE email='""" + message_instance._person_email + "' LIMIT 1"
                    telephone = DbHelper.query_database(sqlquery)
                    vm_instance = VoiceMail.VoiceMail(mailbox=telephone)
                    # Display Summary
                    vm_new = vm_instance.get_inbox(mailbox_type="new")
                    vm_not_read = vm_instance.get_inbox(mailbox_type='not-read')
                    vm_save = vm_instance.get_inbox(mailbox_type='save')
                    log.info('handle_notification() Collecting Spark client details')
                    # Display current voicemail information
                    res = vm_instance.notify(vm_new, vm_not_read, vm_save)
                    if not res:
                        log.error('handle_notification() Unable to get room information {}'.format(telephone))
                    items = vm_instance.get_inbox_detail('new')
                    vm_instance.notify_detail(items)

                return

    except Exception, exception:
        log.exception(exception)


@task(name='process_call', queue='gold', bind=True, default_retry_delay=30, max_retries=3,
      soft_time_limit=settings.max_call_duration)
def process_call(self, message_instance, spark_client, destination, call_type):
    """

    :param self:
    :param spark_client:
    :param destination:
    :param call_type:
    :return:
    """
    twilio_helper(message_instance, spark_client, destination, call_type)
