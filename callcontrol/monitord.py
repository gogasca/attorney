import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/attorney')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/attorney/')
from callcontrol.modules.voicemail.monitor import VoiceMailMonitor
from conf import settings


def VoiceMailProcessor():
    """

    :return:
    """
    print 'Voicemail Monitoring started...'
    vm = VoiceMailMonitor(host=settings.freeswitch_host)
    vm.initialize()


try:
    VoiceMailProcessor()
except KeyboardInterrupt:
    print 'Voicemail Monitoring terminated manually'
