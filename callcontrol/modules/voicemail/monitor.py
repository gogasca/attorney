# coding=utf-8
import logging
import re
import VoiceMail
from conf import logger

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)
from lib.freeswitch import Client
from error.exceptions import ESLError


class VoiceMailMonitor(object):
    """
    VoiceMail Monitor Main class
    """

    def __init__(self, host='127.0.0.1', port='8021', auth='ClueCon'):
        self.host = host
        self.port = port
        self.auth = auth
        self._connection = None
        self._connected = False
        self._events = "BACKGROUND_JOB MESSAGE_WAITING CHANNEL_ANSWER REQUEST_PARAMS"
        print '__init__() VoiceMailMonitor Created...'

    @property
    def connected(self):
        return self._connected

    @connected.setter
    def connected(self, connected):
        self._connected = connected

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, connection):
        self._connection = connection

    def initialize(self):
        """

        :return:
        """
        try:
            client = Client.Client(host=self.host)
            client.connect()
            if client.connected():
                self._connection = client.con
                self._connected = True
                print 'initialize() Connected...'
                self.monitor()

        except ESLError, exception:
            log.exception('initialize() {}'.format(exception))

        except Exception, exception:
            log.exception('initialize() {}'.format(exception))

        finally:
            if client.connected():
                client.disconnect()

    def handle_notification(self, mailbox):
        """

        :param mailbox:
        :return:
        """
        log.info('handle_notification() {}'.format(mailbox))
        match = re.match(r'(\d{11})\@', mailbox)
        if match:
            mailbox = match.group(1)
            log.info('handle_notification() Notifying {}'.format(mailbox))
            vm_instance = VoiceMail.VoiceMail(mailbox=mailbox)
            vm_new = vm_instance.get_inbox(mailbox_type="new")
            vm_not_read = vm_instance.get_inbox(mailbox_type='not-read')
            vm_save = vm_instance.get_inbox(mailbox_type='save')
            log.info('handle_notification() Collecting Spark client details')

            # Notify Spark client for new vm
            if not vm_instance.notify(vm_new, vm_not_read, vm_save, vm_arrived=True):
                log.error('handle_notification() Unable to get room information {}'.format(mailbox))
        else:
            log.error('handle_notification() Unable to get mailbox {}'.format(mailbox))

    def monitor(self):
        """

        :return:
        """
        self.connection.events("plain", self._events)
        print 'monitor() Monitoring started...'
        while True:
            try:
                reply = self.connection.recvEventTimed(1000)
                if reply:
                    ev_name = reply.getHeader('Event-Name')
                    if ev_name == 'CHANNEL_ANSWER':
                        call_from = reply.getHeader('variable_sip_from_user_stripped')
                        call_to = reply.getHeader('variable_sip_to_user')
                        if reply.getHeader("Answer-State") == 'answered':
                            log.info('monitor() {} | CALL {} From: {} To: {} Voicemail port active!'.format(ev_name,
                                                                                                            reply.getHeader(
                                                                                                                "Channel-Call-UUID"),
                                                                                                            call_from,
                                                                                                            call_to))

                    elif ev_name == 'MESSAGE_WAITING':
                        log.info('monitor() {} | MWI {} MWI Notification'.format(ev_name,
                                                                                 reply.getHeader("Core-UUID")))
                        log.info(reply.serialize())
                        mwi_event = reply.getHeader("Update-Reason")
                        mailbox = reply.getHeader("MWI-Message-Account")
                        if mwi_event == 'NEW':
                            log.info('monitor() New voicemail detected')
                            self.handle_notification(mailbox)

                    elif ev_name == 'REQUEST_PARAMS':
                        log.info(reply.serialize())

            except Exception, exception:
                log.exception('Exception {}'.format(exception))
