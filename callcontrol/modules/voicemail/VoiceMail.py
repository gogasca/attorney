import json
import logging
import time
from spark.rooms import Room
from spark.messages import Message
from lib.freeswitch import XmlRpcClient
from conf import settings
from conf import logger

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class VoiceMail(object):
    def __init__(self, mailbox):
        self._mailbox = mailbox

    @property
    def mailbox(self):
        return self._mailbox

    @mailbox.setter
    def mailbox(self, mailbox):
        self._mailbox = mailbox

    def notify(self, vm_new, vm_not_read, vm_save, vm_arrived=False):
        """

        :param vm_new:
        :param vm_not_read:
        :param vm_save:
        :param vm_arrived:
        :return:
        """
        person_id, email, room_id = Room.Room().get_rooms(telephone=self._mailbox)
        if person_id and email and room_id:
            message_instance = Message.Message()
            message_instance._person_email = email
            message_instance._person_id = person_id
            message_instance._room_id = room_id
            if vm_arrived:
                message_instance.send_notification('**You have a new voicemail!**', markdown=True)
            message_instance.send_notification('**Voicemail summary**', markdown=True)
            message_instance.send_notification('- New voicemails ({})'.format(vm_new), markdown=True)
            message_instance.send_notification('- Unread voicemails ({})'.format(vm_not_read), markdown=True)
            message_instance.send_notification('- Saved voicemails ({})'.format(vm_save), markdown=True)
            return True
        return False

    def notify_detail(self, vm_items):
        """

        :param vm_items:
        :return:
        """
        person_id, email, room_id = Room.Room().get_rooms(telephone=self._mailbox)
        if person_id and email and room_id:
            message_instance = Message.Message()
            message_instance._person_email = email
            message_instance._person_id = person_id
            message_instance._room_id = room_id
            if len(vm_items) > 0:
                message_instance.send_notification('**Voicemail details:**', markdown=True)
                for vm in vm_items:
                    message_instance.send_notification(self.format_output(vm), markdown=True)

    @staticmethod
    def format_output(item):
        """

        {'duration': u'6', 'timestamp': '2016-12-14 10:53:38', 'caller': u'Anonymous', 'url': u'http://voicemail.parzee.io/14082186575/msg_60423b6a-7f9a-4a05-aa94-dfee1cb77de0.wav}'
        - **2016-12-14 10:53:38** | **6 sec** | **+14082186575** | [Voicemail message](http://voicemail.parzee.io/14082186575/msg_60423b6a-7f9a-4a05-aa94-dfee1cb77de0.wav)

        :param item:
        :return:
        """
        result = '- '
        if item['timestamp']:
            result += '`' + item['timestamp'] + '` | '
        if item['duration']:
            label = ' secs'
            result += '`' + str(item['duration']) + label + '` | '
        if item['caller']:
            result += '`' + item['caller'] + '` | '
        if item['url']:
            result += '[Voicemail message](' + item['url'] + ')'

        return result

    def get_inbox(self, mailbox_type=None):
        """

        :param mailbox:
        :param mailbox_type:
        :return:
        """

        if mailbox_type in ["new", "not-read", "save"]:

            assert isinstance(self.mailbox, str)

            params = "xml default " + settings.freeswitch_host + " " + self.mailbox + " inbox " + mailbox_type
            server = XmlRpcClient.XMLRpcClientImpl(host=settings.freeswitch_host)
            log.info(params)
            res = server.send_command("vm_fsdb_msg_list", params)
            if res:
                try:
                    res = json.loads(res)
                    log.info(
                        'get_inbox() inbox: {} | {} messages: {}'.format(self.mailbox, mailbox_type,
                                                                             res["VM-List-Count"]))
                    return res["VM-List-Count"]
                except ValueError, exception:
                    log.exception('get_inbox() {}'.format(exception))
            else:
                log.exception('get_inbox() Unable to get messages for: {}'.format(self.mailbox))
        else:
            log.error('get_inbox() Unable to connect to VM server for {} '.format(self.mailbox))

    def get_inbox_detail(self, mailbox_type):
        """

        :param mailbox_type:
        :return:
        """
        if mailbox_type in ["new", "not-read", "save"]:

            assert isinstance(self.mailbox, str)

            params = "xml default " + settings.freeswitch_host + " " + self.mailbox + " inbox " + mailbox_type
            server = XmlRpcClient.XMLRpcClientImpl(host=settings.freeswitch_host)
            log.info(params)
            res = server.send_command("vm_fsdb_msg_list", params)
            if res:
                try:
                    res = json.loads(res)
                    log.info(
                        'get_inbox_detail() inbox: {} | {} messages: {}'.format(self.mailbox, mailbox_type,
                                                                             res["VM-List-Count"]))
                    total = res["VM-List-Count"]
                    items = list()
                    log.info(res)
                    log.info(res["VM-List-Count"])
                    for item in xrange(0, int(total)):
                        voicemail_result = dict()
                        voicemail = res["VM-List-Message-" + str(item + 1) + "-UUID"]
                        log.info('get_inbox_detail() Looking up VM: {} via vm_fsdb_msg_get'.format(voicemail))
                        result = server.send_command("vm_fsdb_msg_get",
                                                     "xml default " + settings.freeswitch_host + " " + self.mailbox + " " + voicemail)
                        j_result = json.loads(result)
                        for k, v in j_result.items():
                            if k in ['VM-Message-Caller-Number', 'VM-Message-Duration', 'VM-Message-File-Path',
                                     'VM-Message-Received-Epoch']:
                                voicemail_result[k] = v

                        items.append(voicemail_result)

                    return self.format_voicemail(items)

                except ValueError, exception:
                    log.exception('get_inbox_detail() {}'.format(exception))
            else:
                log.exception('get_inbox_detail() Unable to get messages for: {}'.format(self.mailbox))
        else:
            log.error('get_inbox_detail() Unable to connect to VM server for {} '.format(self.mailbox))

    @staticmethod
    def format_voicemail(vm_list):
        """

        :param vm_list:
        :return:
        """
        vm_list_format = list()

        for vm in vm_list:
            vm_item = dict()
            if vm['VM-Message-Received-Epoch']:
                vm_item['timestamp'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(vm['VM-Message-Received-Epoch'])))
            if vm['VM-Message-Duration']:
                vm_item['duration'] = vm['VM-Message-Duration']
            if vm['VM-Message-Caller-Number']:
                vm_item['caller'] = vm['VM-Message-Caller-Number']
            if vm['VM-Message-File-Path']:
                l = 46 + len(settings.freeswitch_host)
                vm_item['url'] = settings.freeswitch_web + vm['VM-Message-File-Path'][l:]
            vm_list_format.append(vm_item)

        return vm_list_format