import logging
from api.version1_0.database import DbHelper
from conf import settings, logger

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


def get_user_status(person_email):
    """
        person_email
    """
    # Check user is licensed
    person = DbHelper.query_database(
        sqlquery="SELECT COUNT(*) FROM persons WHERE email='""" + person_email + "' LIMIT 1")
    if person == 0:
        return settings.USER_NOT_LICENSED

    # Check user is registered
    sqlquery = """SELECT parzee_uri FROM persons WHERE email='""" + person_email + "' LIMIT 1"
    parzee_uri = DbHelper.query_database(sqlquery)

    # Person Parzee SIP URI Exists
    if parzee_uri:
        if parzee_uri.startswith(settings.SIP_SCHEME):
            log.info('api() | Valid SIP URI: {}'.format(parzee_uri))
            return settings.USER_REGISTERED
        else:
            log.error('api() | Invalid SIP URI: {}'.format(parzee_uri))
            return settings.USER_INVALID_REGISTRATION

    else:
        log.error('api() | Parzee SIP URI not found for email: {}'.format(person_email))
        log.warn('api() | User is not registered... {}'.format(person_email))
        return settings.USER_NOT_REGISTERED
