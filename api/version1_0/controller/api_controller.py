import numbers
from functools import wraps

from flask import (
    request,
    abort
)

from conf import settings


# =========================================================
# Validate Request comes in valid JSON form
# =========================================================

def validate_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except Exception:
            msg = "Request must be a valid JSON. Use Content-Type: application/json"
            abort(400, description=msg)
        return f(*args, **kw)

    return wrapper


def person_information(request):
    """

    :param request:
    :return:
    """
    try:
        if request:
            person_info = dict()
            if all(k in request for k in ("spark_email", "phone_number", "vm_enabled", "vm_password")):
                person_info['spark_email'] = request["spark_email"]
                person_info['phone_number'] = request["phone_number"]
                person_info['vm_enabled'] = request["vm_enabled"]
                person_info['vm_password'] = request["vm_password"]
            return person_info
        else:
            raise ValueError
    except TypeError, e:
        print e


def webhook_notification(request):
    """

    :param request:
    :return:
    """
    try:
        if request:
            message_info = dict()
            # {u'resource': u'messages', u'name': u'call', u'created': u'2016-09-10T04:57:27.361Z', u'id': u'Y2lzY29zcGFyazovL3VzL1dFQkhPT0svNzA4MmQzOTItNzYxZi00NmQwLTgwYmEtZDhjNGVlYzUxZTEy', u'targetUrl': u'http://d4080c3d.ngrok.io/api/1.0/webhook', u'actorId': u'Y2lzY29zcGFyazovL3VzL1BFT1BMRS9kNjMyMTdiNS05MzgyLTRmN2MtYjMyZi0xM2FiZjRjYTNkNjQ', u'data': {u'roomType': u'direct', u'created': u'2016-09-12T01:32:59.860Z', u'personId': u'Y2lzY29zcGFyazovL3VzL1BFT1BMRS9kNjMyMTdiNS05MzgyLTRmN2MtYjMyZi0xM2FiZjRjYTNkNjQ', u'personEmail': u'gonzalo@parzee.com', u'roomId': u'Y2lzY29zcGFyazovL3VzL1JPT00vMmU2OWVhYTAtOTExZC0zYzRmLWE2MjgtNzc5ZjM5OGVkYWRj', u'id': u'Y2lzY29zcGFyazovL3VzL01FU1NBR0UvZDZmNDUxNDAtNzg4OC0xMWU2LWJhNzMtYzVlNTZkNTU5OWM2'}, u'event': u'created'}

            message_info['message_id'] = request["data"]["id"]
            message_info['person_id'] = request["data"]["personId"]
            message_info['person_email'] = request["data"]["personEmail"]
            message_info['room_id'] = request["data"]["roomId"]
            return message_info
        else:
            raise ValueError
    except TypeError, e:
        print e


def twilio_callinfo(request):
    """

    :param json_request:
    :return:
    """
    try:
        if request:
            call_info = dict()
            call_info['to'] = request.form['Called']
            call_info['country'] = request.form['CallerCountry']
            call_info['callsid'] = request.form['CallSid']
            return call_info
        else:
            raise ValueError
    except TypeError, e:
        print e
