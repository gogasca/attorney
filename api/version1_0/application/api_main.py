import json
import logging
import time
import re
import warnings

import flask_restful
from twilio import twiml
from twilio.rest import TwilioRestClient
from api.version1_0.database import DbHelper

warnings.filterwarnings("ignore")

# =========================================================
# Telephonist imports
# =========================================================

from celery import Celery
from conf import settings, logger
from spark.messages import Message
from spark.rooms import Room
from spark.calls import Call
from utils import call_helper
# =========================================================
# Flask Application imports and database
# =========================================================

from api.version1_0.authentication import authenticator
from api.version1_0.controller import api_controller
from api.version1_0.database import Model, ModelOperations
from flask import current_app
from flask import jsonify, request, Response
from flask_restful import Resource
from sqlalchemy import desc

# =========================================================
# API Controller
# =========================================================

api = flask_restful.Api
log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


# =========================================================
# API Echo information
# =========================================================

class ApiBase(Resource):
    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET API
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())
            if request.headers['Content-Type'] == 'application/json':
                # =========================================================
                # Send API version information
                # =========================================================
                log.info('api() | GET | Version' + settings.api_version)
                response = json.dumps('version: ' + settings.api_version)
                resp = Response(response, status=200, mimetype='application/json')
                return resp

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class CallInstance(Resource):
    def get(self):
        pass

    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            start = time.time()
            call_instance = Call.Call()
            call_instance.id = ModelOperations.insert_call(call_instance.uuid)
            response = twiml.Response()
            client = TwilioRestClient(account=settings.TWILIO_ACCOUNT, token=settings.TWILIO_TOKEN)

            # Obtain call information from Dial callback
            call_sid = request.form['CallSid']
            pstn_number = request.args.get('Pstn')
            sip_uri = request.args.get('SipUri')

            # Check call type and destination:
            if pstn_number:
                log.info('call() PSTN Number: {} Spark client: {}'.format(pstn_number, call_sid))
                destination = call_helper.get_destination(destination=pstn_number)
                log.info('call() PSTN Destination: {}', destination)
            if sip_uri:
                log.info('call() SIP URI: {} Spark client: {}'.format(sip_uri, call_sid))
                destination = sip_uri

            response.pause(length=1)
            response.say("Connecting your call...")
            response.dial(hangupOnStar=True).conference(maxParticipants=2, beep=False, endConferenceOnExit=True,
                                                        name=call_sid)
            call = client.calls.create(to=destination,
                                       from_=settings.TWILIO_FROM,
                                       url=settings.BASE_URL + '/conference/' + call_sid)
            if pstn_number:
                log.info('call() PSTN Number: {} CallSid: {}'.format(pstn_number, call.sid))
            if sip_uri:
                log.info('call() SIP URI: {} CallSid: {}'.format(pstn_number, call.sid))

            end = time.time()
            log.info('call() | Call creation took: {} ms'.format(end - start))

            return Response(str(response), mimetype='application/xml')

        except Exception, e:
            log.exception(e)


class Conference(Resource):
    def get(self):
        pass

    def post(self, conference_name):
        """

        :param conference_name:
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            response = twiml.Response()
            response.pause(length=2)
            response.dial(hangupOnStar=True).conference(beep=False, endConferenceOnExit=True, name=conference_name)
            return Response(str(response), mimetype='application/xml')
        except Exception, e:
            log.exception(e)


class CallStatus(Resource):
    def get(self):
        pass

    def post(self):
        try:
            res = Response(status=200, mimetype='text/xml')
            return res
        except Exception, e:
            log.exception(e)


class Echo(Resource):
    """Used for verifying API status"""

    # @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | GET | Received request for Echo')
        response = json.dumps('Echo: GET. Parzee works')
        resp = Response(response, status=200, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def delete(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | DELETE | Received request for Echo')
        response = json.dumps('Echo: DELETE. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def post(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | POST | Received request for Echo')
        response = json.dumps('Echo: POST. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def put(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | PUT | Received request for Echo')
        response = json.dumps('Echo: PUT. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp


class CodeRegistration(Resource):
    def get(self):
        """
        :return:
        """
        log.info(request.remote_addr + ' ' + request.__repr__())
        response = twiml.Response()

    def post(self):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            response = twiml.Response()
            spark_uri = request.form['Caller']
            registration = False
            # Whitelisting Validate call comes from Spark
            if not ('ciscospark.com' in spark_uri):
                log.error('CodeRegistration() Caller: {} not in valid domain'.format(spark_uri))
                response.hangup()
            else:
                person_id = None
                digits = request.form['Digits']
                response.pause(length=1)
                # Extract Valid code and person id
                sqlquery = """SELECT code, person_id FROM registrations WHERE code='""" + str(
                    digits) + "' AND status=0 LIMIT 1"
                query_result = DbHelper.query_multiple_database(sqlquery)
                if query_result:
                    if len(query_result) == 2:
                        person_id = query_result[1]

                if digits and person_id and spark_uri:
                    log.info('CodeRegistration() Caller: {} Code: {}'.format(spark_uri, digits))
                    response.say(
                        'Congratulations!. Registration is completed. You can place calls now. Thank you!')
                    registration = True
                else:
                    log.error('CodeRegistration() Caller: {} entered invalid code: {}'.format(spark_uri, digits))
                    response.say('Code: ' + " ".join(digits) + ' is invalid. Please try again')
                    response.redirect(url='/api/1.0/register', method="POST")
            return Response(str(response), mimetype='application/xml')
        except Exception, e:
            log.exception('CodeRegistration() {}'.format(e))
        finally:
            if registration:
                log.info('CodeRegistration() Marking code {} as valid'.format(digits))
                #  Update code status
                sqlquery = """UPDATE registrations SET status=1 WHERE code='""" + str(digits) + "'"
                DbHelper.update_database(sqlquery=sqlquery)

                #  Update person sip URI
                sqlquery = """UPDATE persons SET spark_uri='""" + spark_uri + """' WHERE id=""" + str(person_id)
                log.info(sqlquery)
                DbHelper.update_database(sqlquery=sqlquery)

                #  Generate Parzee SIP URI
                sqlquery = """SELECT telephone FROM persons WHERE id=""" + str(person_id) + " LIMIT 1"
                log.info(sqlquery)
                telephone = DbHelper.query_database(sqlquery)

                if telephone:
                    parzee_uri = settings.SIP_SCHEME + telephone + settings.PARZEE_URI
                    sqlquery = """UPDATE persons SET parzee_uri='""" + parzee_uri + """' WHERE id=""" + str(person_id)
                    DbHelper.update_database(sqlquery)
                    person_id, email, room_id = Room.Room().get_rooms(telephone=telephone)
                    if person_id and email and room_id:
                        message_instance = Message.Message()
                        message_instance._person_email = email
                        message_instance._person_id = person_id
                        message_instance._room_id = room_id
                        message_instance.send_notification(
                            'Welcome to Parzee! Registration is completed. Parzee URI: **{}**'.format(parzee_uri),
                            markdown=True)
                    else:
                        log.error('Unable to get room information')
                    # Update Kamailio record
                    DbHelper.insert_to_kamailio(
                        sqlquery='INSERT INTO dbaliases (alias_username, alias_domain, username, domain)',
                        content="'"
                                + str(telephone) + "','"
                                + settings.sip_domain + "','"
                                + spark_uri.split('@')[0][4:] + "','"
                                + spark_uri.split('@')[1] + settings.call_params + "'")


class Registration(Resource):
    def get(self):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            response = twiml.Response()
            response.pause(length=1)
            with response.gather(numDigits=4, action='/api/1.0/confirm', method="POST") as gather:
                gather.say('Thank you for calling Parzee. Please enter your registration code , , ,', loop="1")
            response.pause(length=3)
            response.redirect(url='/api/1.0/register', method="POST")
            return Response(str(response), mimetype='application/xml')
        except Exception, e:
            log.exception('CodeRegistration() {}'.format(e))

    def post(self):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            response = twiml.Response()
            spark_uri = request.form['Caller']
            log.info('Registration() request from: %s' % spark_uri)
            # Verify if user is already registered
            sqlquery = """SELECT COUNT(id) FROM persons WHERE spark_uri='""" + str(spark_uri) + "'"
            query_result = DbHelper.query_database(sqlquery)
            response.pause(length=1)
            if query_result:
                if query_result > 0:
                    response.say(
                        'Your number is already registered. If you cannot place calls please contact Tech Support. '
                        'Thank you')
                    return Response(str(response), mimetype='application/xml')
            # Ask for Registration code
            with response.gather(numDigits=4, action='/api/1.0/confirm', method="POST") as gather:
                gather.say('Thank you for calling Parzee. Please enter your registration code then press pound')
            response.pause(length=5)
            response.redirect(url='/api/1.0/register', method="POST")
            return Response(str(response), mimetype='application/xml')
        except Exception, e:
            log.exception(e)


class Status(Resource):
    @authenticator.requires_auth
    def get(self, task_id):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            log.info('api() | Creating task: ' + task_id)
            start = time.time()
            celery = Celery()
            celery.config_from_object('conf.celeryconfig')
            log.info('api() | Status received request for Task: ' + str(task_id))
            task = celery.AsyncResult(task_id)

            # =========================================================
            # Verify task exists and match
            # =========================================================

            if task and len(task_id) == 10:
                log.info(task.state)

                if task.state == 'PENDING':
                    # job did not start yet
                    response = {
                        'state' : task.state,
                        'status': 'Task is in progress...'

                    }
                elif task.state != 'FAILURE':
                    response = {
                        'state' : task.state,
                        'status': str(task.info),
                    }
                    if task.info:
                        if 'result' in task.info:
                            response['result'] = task.info['result']

                else:
                    response = {
                        'state' : task.state,
                        'status': str(task.info),  # this is the exception raised
                    }

                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                return jsonify(response)

            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class PersonList(Resource):
    """

    """

    def put(self):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

        except Exception, exception:
            log.exception(exception.__repr__())
            response = jsonify({'Error': 'Please try again later'})
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    def get(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            persons = Model.Persons.query.order_by(desc(Model.Persons.phone_number)).limit(
                settings.items_per_page).all()

            response = []

            if persons:
                values = ['parzee_uri', 'spark_email', 'phone_number']
                response = [{value: getattr(d, value) for value in values} for d in persons]

            return jsonify(persons=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = jsonify({'Error': 'Please try again later'})
            resp = Response(response, status=500, mimetype='application/json')
            return resp

    def post(self):
        """
        spark_email
        phone_number
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            # Get dictionary with fields
            person_info = api_controller.person_information(request.json)
            log.info(person_info)
            # Verify spark_email and phone number are provisioned
            if all(k in person_info for k in ("spark_email", "phone_number", "vm_enabled", "vm_password")):
                existing_user = Model.Persons.query.filter(
                    Model.Persons.spark_email == person_info["spark_email"] or Model.Persons.phone_number ==
                    person_info["phone_number"] or Model.Persons.parzee_uri == person_info[
                        "phone_number"] + "@" + settings.sip_domain).first()

                log.info('person() Processing new person: {} | {} | VoiceMail: {}'.format(person_info["spark_email"],
                                                                                          person_info["phone_number"],
                                                                                          person_info["vm_enabled"]))
                if not existing_user:
                    log.info('person() Inserting new person: {}'.format(person_info["spark_email"]))
                    person_instance = ModelOperations.insert_person(phone_number=person_info["phone_number"],
                                                                    spark_email=person_info["spark_email"],
                                                                    vm_enabled=person_info["vm_enabled"],
                                                                    vm_password=person_info["vm_password"])
                else:
                    response = jsonify({'Error': "Person: '{}' already exists.".format(
                        person_info["spark_email"])})
                    response.status_code = 409
                    return response
            else:
                response = jsonify({'Error': "Person: '{}' incomplete args"})
                response.status_code = 400
                return response

            response = jsonify({'parzee_uri': person_info["phone_number"] + "@" + settings.sip_domain})
            response.headers['Location'] = api.url_for(api(current_app),
                                                       Person,
                                                       person_id=person_instance,
                                                       _external=True,
                                                       _scheme=settings.api_scheme)
            response.status_code = 202
            return response

        except Exception, exception:
            log.exception(exception.__repr__())
            response = jsonify({'Error': 'Please try again later'})
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class Person(Resource):
    """

    """

    def get(self, person_id):
        """

        :param id:
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Get JOB by reference
            # =========================================================
            log.info('person() | Finding Person: ' + str(person_id))
            person = Model.Persons.query.filter(Model.Persons.id == person_id).first()

            if person:
                log.info('person() | Person found: ' + str(person_id))
                return jsonify(person.serialize())

            log.warn('person() | Person not found: ' + str(person_id))
            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class WebHook(Resource):
    """

    """

    def get(self):
        pass

    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            webhook_notification = api_controller.webhook_notification(request.json)

            if 'message_id' in webhook_notification:
                log.info('webhook() Processing webhook message')
                message_instance = Message.Message(webhook_notification['message_id'])
                message_instance.text = message_instance.get_message()
                # Process messages with /call +14082186575|/conference A B | /register
                if message_instance.text:
                    if re.match('/call\s+|/voicemail|/inbox|/register|/activate\s+|/conference\s+|/help',
                                message_instance.text):
                        # Ignore when we print info help
                        if settings.spark_info_help in message_instance.text:
                            res = Response(status=200, mimetype='text/xml')
                            return res
                        else:
                            message_instance.id = ModelOperations.insert_message(webhook_notification['message_id'],
                                                                                 webhook_notification['person_email'],
                                                                                 message_instance.text)

                            log.info('webhook() Call request found')
                            celery = Celery()
                            celery.config_from_object("conf.celeryconfig")
                            celery.send_task('process_message',
                                             exchange='attorney',
                                             queue='gold',
                                             routing_key='attorney.gold',
                                             kwargs={'message_instance': message_instance,
                                                     'webhook_notification': webhook_notification},
                                             task_id=webhook_notification['message_id'],
                                             retries=3)
            res = Response(status=200, mimetype='text/xml')
            return res

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp
