import logging
import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/attorney')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/attorney/')
import warnings
warnings.filterwarnings("ignore")
from conf import settings, logger
from utils import banner

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)

# =========================================================
# Flask Application imports and database
# =========================================================
from flask import Flask
import flask_restful
from api.version1_0.database import Model
from api_main import ApiBase, CallInstance, Conference, CallStatus, CodeRegistration, Echo, Status, Person, PersonList, Registration, WebHook
from werkzeug.contrib.fixers import ProxyFix


# =========================================================
# Flask Main Application
# =========================================================

api_app = Flask(__name__)  # Flask Application
api_app.config.from_pyfile("../../../conf/settings.py")  # Flask configuration

attorney_api = flask_restful.Api(api_app)  # Define API
# limiter = Limiter(api_app, key_func=get_remote_address, global_limits=["10 per second"])
db = Model.db.init_app(api_app)  # Initialize database

# =========================================================
# API Definition
# =========================================================

attorney_api.add_resource(ApiBase, settings.api_base_url)
attorney_api.add_resource(CallInstance, '/api/1.0/call')
attorney_api.add_resource(CallStatus, '/api/1.0/callstatus')
attorney_api.add_resource(Conference, '/api/1.0/conference/<string:conference_name>')
attorney_api.add_resource(Echo, '/api/1.0/echo')
attorney_api.add_resource(PersonList, '/api/1.0/person')
attorney_api.add_resource(Person, '/api/1.0/person/<int:person_id>')
attorney_api.add_resource(Status, '/api/1.0/status/<string:task_id>')
attorney_api.add_resource(Registration, '/api/1.0/register')
attorney_api.add_resource(CodeRegistration, '/api/1.0/confirm')
attorney_api.add_resource(WebHook, '/api/1.0/webhook')

# =========================================================
# API Proxy WSGi for gunicorn
# =========================================================

api_app.wsgi_app = ProxyFix(api_app.wsgi_app)

# =========================================================
# API Logs
# =========================================================

log.info('Initializing Attorney API >>>')
banner.horizontal(' Attorney API v0.1 ')