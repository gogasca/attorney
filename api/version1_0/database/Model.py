# coding: utf-8
import datetime
from collections import OrderedDict

from conf import settings
from sqlalchemy import Boolean, Column, DateTime, Integer, String, text, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect

Base = declarative_base()
metadata = Base.metadata
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from sqlalchemy import Column, Date, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base


class Serializer(object):
    """
        Serialize information from database to a JSON Dictionary
    """

    def serialize(self):
        return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]


class AutoSerialize(object):
    """
        Mixin for retrieving public fields of model in json-compatible format'
    """
    __public__ = None

    def get_public(self, exclude=(), extra=()):
        "Returns model's PUBLIC data for jsonify"
        data = {}
        keys = self._sa_instance_state.attrs.items()
        public = self.__public__ + extra if self.__public__ else extra
        for k, field in keys:
            if public and k not in public: continue
            if k in exclude: continue
            value = self._serialize(field.value)
            if value:
                data[k] = value
        return data

    @classmethod
    def _serialize(cls, value, follow_fk=False):
        if type(value) in (datetime,):
            ret = value.isoformat()
        elif hasattr(value, '__iter__'):
            ret = []
            for v in value:
                ret.append(cls._serialize(v))
        elif AutoSerialize in value.__class__.__bases__:
            ret = value.get_public()
        else:
            ret = value

        return ret


class DictSerializable(object):
    """

    """

    def _asdict(self):
        result = OrderedDict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result


class Call(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'calls'

    id = Column(Integer, primary_key=True, server_default=text("nextval('calls_id_seq'::regclass)"))
    uuid = Column(String(128), nullable=False)
    callsid = Column(String(128), nullable=False)
    call_start = Column(Date)
    call_end = Column(Date)
    owner_id = Column(Integer)
    customer_id = Column(Integer)

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d

    def __init__(self, uuid):
        """

        :param message_id:
        :param text:
        :return:
        """
        self.uuid = uuid


class Customer(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'customer'

    id = Column(Integer, primary_key=True, server_default=text("nextval('customer_id_seq'::regclass)"))
    name = Column(String(128), nullable=False)
    description = Column(String(2048), nullable=False)

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d


class Persons(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'persons'

    id = Column(Integer, primary_key=True, server_default=text("nextval('persons_id_seq'::regclass)"))
    email = Column(String(128), nullable=False)
    telephone = Column(String(64), nullable=False)
    parzee_uri = Column(String(128), nullable=False)
    vm_enabled = Column(Boolean, server_default=text("false"))
    vm_password = Column(String(128), nullable=False)

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d

    def __init__(self, spark_email, phone_number, vm_enabled=False, vm_password=None):
        """

        :param message_id:
        :param text:
        :return:
        """
        self.email = spark_email
        self.telephone = phone_number
        self.parzee_uri = phone_number + settings.PARZEE_URI
        self.vm_enabled = vm_enabled
        self.vm_password = vm_password


class Message(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True, server_default=text("nextval('messages_id_seq'::regclass)"))
    message_id = Column(String(256), nullable=False)
    text = Column(String(2048), nullable=False)
    description = Column(String(2048), nullable=True)
    person_email = Column(String(1024), nullable=True)

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d

    def __init__(self, message_id, person_email, text):
        """

        :param message_id:
        :param text:
        :return:
        """
        self.message_id = message_id
        self.person_email = person_email
        self.text = text
