import logging
import bleach
import psycopg2
from conf import logger
from error import exceptions
from conf import settings

# http://initd.org/psycopg/docs/module.html

log = logger.LoggerManager().getLogger("__app___")
log.setLevel(level=logging.DEBUG)


class Db():
    """
        Database handling
    """

    def __init__(self,
                 server=settings.dbhost,
                 username=settings.dbusername,
                 password=settings.dbpassword,
                 database=settings.dbname,
                 port=settings.dbport, **kwargs):
        """

        :param server:
        :param username:
        :param password:
        :param database:
        :param port:
        :param kwargs:
        :return:
        """
        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.dsn = 'dbname=%s host=%s port=%d user=%s password=%s' % (self.database,
                                                                      self.server,
                                                                      self.port,
                                                                      self.username,
                                                                      self.password)
        self.conn = None

    def initialize(self, dsn=None, **kwargs):
        """

        :param dsn:
        :param kwargs:
        :return:
        """

        if self.dsn:
            self.conn = psycopg2.connect(self.dsn)
            return True
        elif dsn:
            self.conn = psycopg2.connect(dsn)
            return True
        else:
            log.info('DB initialize() DB not initialized')
            raise exceptions.DatabaseParameters('Invalid dsn.')

    def insert(self, query, content):
        """

        :param query:
        :param content:
        :return:
        Example:

            INSERT INTO registrations (code, person_id, creation, status) VALUES ( '5430', '1', now(), 0')
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                content = bleach.clean(content)

                final_query = query + " VALUES (" + content + ") RETURNING id;"
                log.info('DB insert() Executing SQL query: ' + final_query)
                cur.execute(final_query)
                id_of_new_row = cur.fetchone()[0]
                self.conn.commit()
                return id_of_new_row

            else:
                log.error('DB insert() Invalid DB parameters No connection to DB')
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise

        except Exception as exception:
            log.exception('DB.update() Unable to insert query: {}'.format(exception))
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

    def update(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                self.conn.commit()
            else:
                log.error('DB update() Invalid db parameters')
                return
        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.update() Unable to query: {}'.format(exception))
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

    def query_multiple(self,query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                # A commit may go here...
                result = cur.fetchone()
                if result:
                    return result
            else:
                log.error('DB.query_multiple() Invalid db parameters')
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query_multiple() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.query_multiple() Unable to query: {}'.format(exception))
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

    def query(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                # A commit may go here...
                result = cur.fetchone()
                if result:
                    return result[0]
            else:
                log.error('DB.query() Invalid db parameters')
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.query() Unable to query: {}'.format(exception))
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

    def clean_up(self):
        """
        Clean up idle connections
        :return:
        """
        query = "select pg_terminate_backend(pid) from pg_stat_activity where usename = '" + settings.dbusername + \
                "' and state = 'idle' and query_start < current_timestamp - interval '5 minutes';"
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                # A commit may go here...
                rows = cur.fetchall()
                return rows
            else:
                log.error('DB.query() Invalid Database parameters')
                print 'No connection to Database'
                return

        except psycopg2.ProgrammingError as exception:
            log.exception('DB.query() Database configuration: {}'.format(exception))
            raise
        except Exception as exception:
            log.exception('DB.query() Unable to query: {}'.format(exception))
            raise
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()
