import Model
from Model import db as db
import psycopg2


def insert_call(uuid):
    """

    :param call_uuid:
    :param status:
    :param destination:
    :param request_data:
    :param campaign:
    :return:
    """
    try:
        # uuid, destination, request_data, campaign, start
        call = Model.Call(uuid)
        db.session.add(call)
        db.session.commit()
        return call.id
    except Exception, e:
        db.session.rollback()
        print e
    finally:
        db.session.close()


def insert_person(phone_number, spark_email, vm_enabled, vm_password):
    """

    :param phone_number:
    :param spark_email:
    :return:
    """
    try:
        person = Model.Persons(spark_email, phone_number, vm_enabled, vm_password)
        db.session.add(person)
        db.session.commit()
        return person.id
    except psycopg2.IntegrityError, exception:
        db.session.rollback()
        print exception
    except Exception, e:
        db.session.rollback()
        print e
    finally:
        db.session.close()


def insert_message(message_id, person_email, text):
    """

    :param call_uuid:
    :param status:
    :param destination:
    :param request_data:
    :param campaign:
    :return:
    """
    try:
        # uuid, destination, request_data, campaign, start
        message = Model.Message(message_id, person_email, text)
        db.session.add(message)
        db.session.commit()
        return message.id
    except Exception, e:
        db.session.rollback()
        print e
    finally:
        db.session.close()
