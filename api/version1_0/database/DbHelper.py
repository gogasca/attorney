from api.version1_0.database import Db
from conf import settings
import psycopg2


def query_multiple_database(sqlquery):
    """

    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        return db.query_multiple(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def query_database(sqlquery):
    """

    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        return db.query(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def update_database(sqlquery):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        db.update(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def insert_to_database(sqlquery, content):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        return db.insert(query=sqlquery, content=content)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def insert_to_kamailio(sqlquery, content):
    """

    :rtype : object
    :param sqlquery:
    :return:

    """

    try:
        db = Db.Db()
        db.dsn = None # TODO Improve initialization
        db.initialize(dsn=settings.SQLALCHEMY_KAMAILIO)
        return db.insert(query=sqlquery, content=content)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception
