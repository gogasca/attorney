__author__ = 'gogasca'

# =========================================================
# main application | gunicorn attorney_api:api_app --bind 0.0.0.0:8080
# =========================================================

from application.app import api_app

if __name__ == "__main__":
    api_app.run(debug=True, threaded=True)
